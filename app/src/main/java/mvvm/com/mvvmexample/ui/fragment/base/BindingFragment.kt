package com.ezlo.mvvm.ui.fragment.base

import android.content.Intent
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.NonNull
import android.support.annotation.Nullable
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ezlo.mvvm.business.viewModel.interfaces.base.IFragmentVM


/**
 * Created by Aleks Plekhov on 30/05/2017.
 */

abstract class BindingFragment<VM : IFragmentVM, B : ViewDataBinding> : Fragment() {

    protected abstract fun onCreateViewModel(binding: B): VM
    lateinit var binding: B
    lateinit var viewModel: VM
    abstract fun getVariable(): Int
    abstract fun getLayoutId(): Int
    var savedInstanceState: Bundle? = null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<B>(inflater, getLayoutId(), container, false)
        return binding.root
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.savedInstanceState = savedInstanceState
        viewModel = onCreateViewModel(binding)
        binding.setVariable(getVariable(), viewModel)
        binding.executePendingBindings()
        viewModel.onViewCreated()
    }

    fun resetViewModel() {
        viewModel = onCreateViewModel(binding)
        binding.setVariable(getVariable(), viewModel)
    }

    override fun onStart() {
        viewModel.onStart()
        super.onStart()
    }

    override fun onStop() {
        viewModel.onStop()
        super.onStop()
    }

    override fun onPause() {
        viewModel.onPause()
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        viewModel.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.onDestroy()

    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState?.let {
            viewModel.onSaveInstanceState(outState)
        }
    }

    override fun onViewStateRestored(@Nullable savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        savedInstanceState?.let {
            viewModel.onViewStateRestored(savedInstanceState)
        }
    }

    override fun onDestroyView() {
        viewModel.onDestroyView()
        super.onDestroyView()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        viewModel = onCreateViewModel(binding)
        viewModel.onActivityResult(requestCode, resultCode, data)
    }

    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        viewModel.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


}