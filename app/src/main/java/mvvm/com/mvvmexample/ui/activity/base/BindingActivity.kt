package com.ezlo.mvvm.ui.activity.base

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity

import com.ezlo.mvvm.business.viewModel.interfaces.base.IActivityVM


/**
 * Created by Aleks Plekhov on 29/05/2017.
 */
abstract class BindingActivity<Bind : ViewDataBinding, VM : IActivityVM> : AppCompatActivity() {

    lateinit private var binding: Bind
    lateinit protected var viewModel: VM


    protected abstract fun provideViewModel(): VM
    protected @LayoutRes abstract fun getLayoutId(): Int
    protected @IdRes abstract fun getVariable(): Int


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind()
    }

    fun bind() {
        binding = DataBindingUtil.setContentView<Bind>(this, getLayoutId())

        viewModel = provideViewModel()

        binding.setVariable(getVariable(), viewModel)
        binding.executePendingBindings()
    }


    override fun onStart() {
        super.onStart()
        viewModel.onStart()
    }

    override fun onResume() {
        super.onResume()
        viewModel.onResume()
    }


    override fun onBackPressed() {
        super.onBackPressed()
        viewModel.onBackPressed()
    }


}