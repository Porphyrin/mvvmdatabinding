package com.ezlo.mvvm.ui.activity


import com.ezlo.mvvm.business.viewModel.ExampleActivityVM
import com.ezlo.mvvm.ui.activity.base.BindingActivity
import mvvm.com.mvvmexample.BR

import mvvm.com.mvvmexample.R
import mvvm.com.mvvmexample.databinding.ActivityExampleBinding

class ExampleActivity : BindingActivity<ActivityExampleBinding, ExampleActivityVM>() {

    override fun getLayoutId(): Int = R.layout.activity_example
    override fun getVariable(): Int = BR.exampleViewModel
    override fun provideViewModel(): ExampleActivityVM = ExampleActivityVM(this)


}