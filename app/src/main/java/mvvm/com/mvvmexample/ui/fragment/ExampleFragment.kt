package com.ezlo.mvvm.ui.fragment

import com.ezlo.mvvm.business.viewModel.ExampleFragmentVM
import com.ezlo.mvvm.ui.fragment.base.BindingFragment
import mvvm.com.mvvmexample.BR

import mvvm.com.mvvmexample.R
import mvvm.com.mvvmexample.databinding.FragmentExampleBinding


/**
 * Created by Aleks Plekhov on 30/05/2017.
 */
class ExampleFragment : BindingFragment<ExampleFragmentVM, FragmentExampleBinding>() {

    override fun onCreateViewModel(binding: FragmentExampleBinding): ExampleFragmentVM = ExampleFragmentVM(this)
    override fun getVariable(): Int = BR.viewModel
    override fun getLayoutId(): Int = R.layout.fragment_example


    companion object {
        fun newInstance(): ExampleFragment {
            return ExampleFragment()
        }
    }





}