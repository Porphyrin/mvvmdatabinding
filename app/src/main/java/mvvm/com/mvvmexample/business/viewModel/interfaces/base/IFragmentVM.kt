package com.ezlo.mvvm.business.viewModel.interfaces.base

import android.content.Intent
import android.os.Bundle


/**
 * Created by Aleks Plekhov on 31/05/2017.
 */
interface IFragmentVM {

    fun onViewCreated()
    fun onDestroyView()



    fun onStart(){}

    fun onStop() {}

    fun onDestroy() {}

    fun onPause() {}

    fun onResume() {}

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {}

    fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {}

    fun onSaveInstanceState(outState: Bundle) {}

    fun onViewStateRestored(savedInstanceState: Bundle) {}
}