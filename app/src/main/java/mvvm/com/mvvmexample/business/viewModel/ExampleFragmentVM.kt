package com.ezlo.mvvm.business.viewModel


import com.ezlo.mvvm.business.viewModel.interfaces.base.IFragmentVM
import com.ezlo.mvvm.ui.fragment.ExampleFragment


/**
 * Created by Aleks Plekhov on 30/05/2017.
 */
class ExampleFragmentVM(val fragment: ExampleFragment) : IFragmentVM {

    override fun onViewCreated() {
    }

    override fun onDestroyView() {
    }



}