package com.ezlo.mvvm.business.viewModel

import android.databinding.BaseObservable


import com.ezlo.mvvm.business.viewModel.interfaces.base.IActivityVM
import com.ezlo.mvvm.ui.activity.ExampleActivity


/**
 * Created by Aleks Plekhov on 29/05/2017.
 */
class ExampleActivityVM(var activity: ExampleActivity) : IActivityVM, BaseObservable() {
    override fun onStart() {

    }

    override fun onResume() {

    }

    override fun onBackPressed() {

    }


}